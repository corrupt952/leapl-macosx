#import <Foundation/Foundation.h>
#import "LeapObjectiveC.h"

typedef enum {
    Up,
    Down,
    Left,
    Right,
    Circle,
    Tap
} Gestures;

@interface Gesture : NSObject

@property (nonatomic) Gestures gestures;
@property (nonatomic) NSInteger fingers;
@property (nonatomic) NSInteger hands;


-(id) initWithDirection:(Gestures)dir andFingers:(NSInteger)count;

@end
