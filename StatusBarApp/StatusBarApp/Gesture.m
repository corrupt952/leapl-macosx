#import "Gesture.h"

@implementation Gesture

@synthesize gestures;
@synthesize fingers;

-(id)initWithDirection:(Gestures)dir andFingers:(NSInteger)count{
    if(self = [super init]){
        self.gestures = dir;
        self.fingers = count;
    }
    return self;
}
    
@end
