#import "GestureListener.h"
#import "Gesture.h"

@implementation GestureListener
@synthesize isRightClick;
@synthesize isLeftClick;

- (id)init {
    NSLog(@"Initialized %@", [self className]);
    gestureTimeout = 0;
    isRightClick = false;
    isLeftClick = false;
    return self;
}

- (void) run{
    if (mController == nil) {
        mController = [[LeapController alloc] init];
    }
    [mController addDelegate:self];
    // Set policy
    [mController setPolicyFlags:LEAP_POLICY_BACKGROUND_FRAMES];
    // Enabled gesture
    [mController enableGesture:(LEAP_GESTURE_TYPE_CIRCLE) enable:true];
    NSLog(@"running");
}

- (void) stop {
    [mController removeDelegate];
}

- (void)setGestureEvent:(OnGestureEvent)callback{
    onGesture = callback;
}

- (void)onConnect:(LeapController *)controller{
    NSLog(@"controller connected");
}

- (void)onDisconnect:(LeapController *)controller{
    NSLog(@"Disconnected");
}

- (void)onFrame:(LeapController *)controller{
    LeapFrame *frame = [mController frame:0];
    NSArray *screens = [mController locatedScreens];
    NSArray *pointables = [frame pointables];
    if ([pointables count] > 0 && [screens count] > 0) {
        LeapScreen *screen = [screens objectAtIndex:0];
        LeapVector *location = nil;
        switch ([pointables count]) {
            case 0:
            {
                CGEventRef event = CGEventCreate(NULL);
                CGPoint point = CGEventGetLocation(event);
                CFRelease(event);
                if (isRightClick) {
                    [self PostMouseEvent:kCGMouseButtonRight eventType:kCGEventRightMouseUp fromPoint:point];
                    isRightClick = false;
                }
                if (isLeftClick) {
                    [self PostMouseEvent:kCGMouseButtonLeft eventType:kCGEventLeftMouseUp fromPoint:point];
                    isLeftClick = false;
                }
                break;
            }
            default:
            {
                // Moved cursor
                LeapInteractionBox *iBox = [frame interactionBox];
                LeapPointable *pointable = [pointables objectAtIndex:0];
                LeapVector *tip = [iBox normalizePoint:[pointable stabilizedTipPosition] clamp:true];
                float x = [tip x] * [screen widthPixels];
                float y = [screen heightPixels] - [tip y] * [screen heightPixels];
                location =[[LeapVector alloc] initWithX:x y:y z:0];

                CGPoint point = CGPointZero;
                if (location != nil) {
                    NSPoint vec = NSMakePoint(location.x, location.y);
                    point = vec;

                    CGEventSourceRef source = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState);
                    CGEventRef mouse = CGEventCreateMouseEvent (NULL, kCGEventMouseMoved, vec, 0);
                    CGEventPost(kCGHIDEventTap, mouse);
                    CFRelease(mouse);
                    CFRelease(source);
                }

                if (!CGPointEqualToPoint(point, CGPointZero)) {
                    // Click
                    switch ([self countPointableInTouchZone:pointables]) {
                        case 0:
                        {
                            if (isRightClick) {
                                NSLog(@"Right up.");
                                [self PostMouseEvent:kCGMouseButtonRight eventType:kCGEventRightMouseUp fromPoint:point];
                                isRightClick = false;
                            }
                            if (isLeftClick) {
                                NSLog(@"Left up.");
                                [self PostMouseEvent:kCGMouseButtonLeft eventType:kCGEventLeftMouseUp fromPoint:point];
                                isLeftClick = false;
                            }
                            break;
                        }
                        case 1:
                        {
                            // 左クリック
                            if (!isLeftClick && !isRightClick) {
                                NSLog(@"Left down.");
                                isLeftClick = true;
                                [self PostMouseEvent:kCGMouseButtonLeft eventType:kCGEventLeftMouseDown fromPoint:point];
                            }
                            break;
                        }
                        default:
                        {
                            if (!isRightClick && !isLeftClick) {
                                // 右クリック
                                NSLog(@"Right down.");
                                isRightClick = true;
                                [self PostMouseEvent:kCGMouseButtonRight eventType:kCGEventRightMouseDown fromPoint:point];
                            }
                            break;
                        }
                    }
                }
                break;
            }
        }
        
    }

    // Gestures
    for (LeapGesture *gesture in [frame gestures:nil]) {
        if ([gesture type] == LEAP_GESTURE_TYPE_CIRCLE) {
            LeapCircleGesture *circleGesture = (LeapCircleGesture *)gesture;
            int circleVec;
            if ([[[circleGesture pointable] direction] angleTo:[circleGesture normal]] <= M_PI / 2) {
                circleVec =-1;
            } else {
                circleVec = 1;
            }
            CGEventRef event = CGEventCreateScrollWheelEvent(NULL, kCGScrollEventUnitLine, 1, circleVec, 0);
            CGEventPost(kCGHIDEventTap, event);
        }
    }
}

- (void) gestureDetected:(Gesture *)gesture{
    if ((prevGesture == nil || [prevGesture gestures] != [gesture gestures]) && onGesture != nil) {
        gestureTimeout = 200;
        prevGesture = gesture;
        onGesture(gesture);
    }
}

// TouchZoneにあるPointableの個数を数える
- (int) countPointableInTouchZone:(NSArray *)pointables {
    int count = 0;
    for (LeapPointable *pointable in pointables) {
        if ([pointable touchZone] == LEAP_POINTABLE_ZONE_TOUCHING) {
            count++;
        }
    }
    return count;
}

// マウスイベントを作成し送信
- (void) PostMouseEvent:(CGMouseButton)button eventType:(CGEventType)type fromPoint:(const CGPoint)point {
    CGEventRef event = CGEventCreateMouseEvent(NULL, type, point, button);
    CGEventSetType(event, type);
    CGEventPost(kCGHIDEventTap, event);
    CFRelease(event);
}

- (void)onInit:(LeapController *)controller {
    NSLog(@"onInit %@", [self className]);
}

- (void)onExit:(LeapController *)controller{
    NSLog(@"Exited");
}

@end
