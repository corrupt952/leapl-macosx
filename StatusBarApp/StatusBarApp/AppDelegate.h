#import <Cocoa/Cocoa.h>
#import "GestureListener.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (nonatomic) GestureListener *listener;

@end
