//
//  main.m
//  StatusBarApp
//
//  Created by Kazuki Hasegawa on 2013/10/28.
//  Copyright (c) 2013年 Kazuki Hasegawa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
