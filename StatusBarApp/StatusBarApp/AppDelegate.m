#import "AppDelegate.h"
#import "Gesture.h"
#import "GestureListener.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSMenu *statusMenu;

@end

@implementation AppDelegate
{
    // Instance
    NSStatusItem *_statusItem;
}

@synthesize listener;

- (void)setupStatusItem
{
    NSStatusBar *systemStatusBar = [NSStatusBar systemStatusBar];
    _statusItem = [systemStatusBar statusItemWithLength:NSVariableStatusItemLength];
    [_statusItem setHighlightMode:YES];
    //[_statusItem setTitle:@""];
    NSImage *image = [NSImage imageNamed:@"Icon"];
    NSSize imageSize;
    imageSize.height = 25;
    imageSize.width = 25;
    [image setSize:imageSize];
    [_statusItem setImage:image];
    [_statusItem setMenu:self.statusMenu];

    // Leap
    listener = [[GestureListener alloc] init];
    [listener setGestureEvent:^(Gesture *gesture) { }];
}

- (void) applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    [self setupStatusItem];
}

- (IBAction)LeaplStart:(id)sender {
    [listener run];
}

- (IBAction)LeaplExit:(id)sender {
    [listener stop];
}


@end
