#import <Foundation/Foundation.h>
#import "LeapObjectiveC.h"
#import "Gesture.h"

typedef void (^OnGestureEvent) (Gesture *g);

@interface GestureListener : NSObject<LeapDelegate>{
    LeapController *mController;
    Gesture *prevGesture;
    OnGestureEvent onGesture;
    int gestureTimeout;
}
@property Boolean isRightClick;
@property Boolean isLeftClick;

- (void) run;

- (void) stop;

- (void) setGestureEvent:(OnGestureEvent)callback;

- (void) gestureDetected:(Gesture*)gesture;

- (int) countPointableInTouchZone:(NSArray *)pointables;

- (void) PostMouseEvent:(CGMouseButton) button eventType:(CGEventType) type fromPoint:(const CGPoint) point;

@end
